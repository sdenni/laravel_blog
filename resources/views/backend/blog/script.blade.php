@section('script')
    <script type="text/javascript">
        $('ul.pagination').addClass('no-margin pagination-sm');

        $('#title').on('blur', function(){
          var theTitle = this.value.toLowerCase().trim(),
              slugInput = $('#slug'), 
              theSlug = theTitle.replace(/&/g, '-and-').replace(/[^a-z0-9-]+/g, '-').replace(/\-\-/g, '-').replace(/^-+|-+$/g);

          slugInput.val(theSlug);
        })

        var simplemde1 = new SimpleMDE({ element: $("#excerpt")[0]});
        var simplemde2 = new SimpleMDE({ element: $("#body")[0]});

        $('#datetimepicker1').datetimepicker({
          format: 'YYYY-MM-DD HH:mm:ss',
          showClear: true
        });

        // $('#publish-btn').click(function(e) {
        //   e.preventDefault();

        //     var d = new Date();

        //     var month = '' + (d.getMonth() + 1);
        //     var day = '' + d.getDate();
        //     var year = d.getFullYear(),;
        //     var hour = d.getHours();
        //     var minute = d.getMinutes();

        //     if (month.length < 2) month = '0' + month;
        //     if (day.length < 2) day = '0' + day;
        //     if (hour.length < 2) day = '0' + hour;
        //     if (minute.length < 2) day = '0' + minute;

        //     var timestring = [year, month, day].join('-') +" "+[hour, minute, "00"].join(':');

        //     console.log(timestring);

        //   $('#published_at').val(timestring);
        //   $('#post-form').submit();
        // });


        $('#draft-btn').click(function(e) {
          e.preventDefault();
          $('#published_at').val("");
          $('#post-form').submit();
        });
    </script>    
@endsection