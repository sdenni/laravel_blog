<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // reset the users table
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate();

        //generate 3 users/author
        $faker = Factory::create();

        DB::table('users')->insert([
            [
                'name' => "John Doe",
                'slug' => "john-doe",
                'email' => "johndoe@test.com",
                'password' => bcrypt('123123'),
                'bio' => $faker->text(rand(250, 300)),
            ],
            [
                'name' => "Someone",
                'slug' => "someone",
                'email' => "someone@test.com",
                'password' => bcrypt('123123'),
                'bio' => $faker->text(rand(250, 300)),
            ],
            [
                'name' => "Bill Gates",
                'slug' => "bill-gates",
                'email' => "bill@test.com",
                'password' => bcrypt('123123'),
                'bio' => $faker->text(rand(250, 300)),
            ]
        ]);
    }
}
