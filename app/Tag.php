<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function posts($value='') {
        return $this->belongsToMany(Post::class);
    }

    public function getRouteKeyName(){
        return 'slug';
    }
}
